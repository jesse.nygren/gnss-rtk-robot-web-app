let languages = [];
const flagSize = 64;
let currentLanguage = null;

//Navbar elements
const mapButton = document.getElementById("map");
const helpButton = document.getElementById("about");
const projectButton = document.getElementById("team");
const logoutButton = document.getElementById("logout");
//
const mapStyle = document.getElementById("mapStyle");
const normalMap = document.getElementById("normal-map");
const satelliteMap = document.getElementById("satellite-map");

//
const selectRobot = document.getElementById("selectRobot");
const noSelection = document.getElementById("noSelection");
const unselect = document.getElementById("unselectRobot");


//Waypoint tools
const waypointText = document.getElementById("waypoints");
const wpClearButton = document.getElementById("wpClear");
const wpPushButton = document.getElementById("wpPush");

// Text variables
let clearText = "";
let pushText = "";
let waypointsText = "";

showLanguages();


function showLanguages() {
    db.collection('language').get().then((snapshot) => {
        snapshot.docs.forEach(doc => {

            console.log(doc.id);
            languages.push(doc.id);
            renderLanguagesToList(doc);
        })
    });
}

function renderLanguagesToList(doc) {
    if (doc.id == "gb") {
        localize(doc);
        currentLanguage = doc;
    }
    const parent = document.getElementById("countries");
    const li = document.createElement("li");
    const img = document.createElement("img");
    const radio = document.createElement("input");

    img.setAttribute("src", "https://www.countryflags.io/" + doc.id + "/shiny/" + flagSize + ".png");
    img.style.cursor = "pointer";
    radio.setAttribute("type", "radio");
    radio.setAttribute("name", "selectedLanguage");
    radio.id = doc.id;

    if (radio.id == "gb") {
        radio.setAttribute("checked", "checked");
    }
    radio.style.marginLeft = "40px";
    li.appendChild(img);
    li.appendChild(radio);
    parent.appendChild(li);

    img.addEventListener("click", function () {
        localize(doc);
        radio.setAttribute("checked", "checked");
        currentLanguage = doc;
    })
    radio.addEventListener("change", function () {
        if (this.checked) {
            localize(doc);
            currentLanguage = doc;
        }
    })

}

function localize(doc) {
    mapButton.innerHTML = doc.data().mapBtn;
    helpButton.innerHTML = doc.data().helpBtn;
    projectButton.innerHTML = doc.data().projectBtn;
    logoutButton.innerHTML = doc.data().logoutBtn;

    waypointsText = doc.data().wpText;
    clearText = doc.data().wpClear;
    pushText = doc.data().wpPush;

    toolBtn.innerHTML = "< " + doc.data().toolBtn + " <";

    mapStyle.innerHTML = doc.data().mapStyle;
    // normalMap.innerHTML = doc.data().normalMap;
    // satelliteMap.innerHTML = doc.data().satelliteMap;

    selectRobot.innerHTML = doc.data().selectRobot;
    noSelection.innerHTML = doc.data().noSelection;
    unselect.innerHTML = doc.data().unselect;
}