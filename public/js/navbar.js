const buttons = document.getElementById("navbar-content").children;
const homepage = "map-page";
let clicked = false;

for (var i = 0; i < buttons.length; i++) {
    const button = document.getElementById(buttons[i].id);
    if (buttons[i].id == "logout") {
        button.addEventListener('click', function () {
            logout();
        });
    } else if (buttons[i].id == "home") {
        button.addEventListener('click', function () {
            showPage(homepage);
        });
    } else if (buttons[i].id == "sideOpen") {
        button.addEventListener('click', function () {
            if (clicked != true) {
                openNav();
            } else {
                closeNav();
                
            }
        });
    } else if (buttons[i].id == "emergencyButton"){

    } 
    else {
        console.log(button.id);
        button.addEventListener('click', function () {
            showPage(button.id + "-page");
        });

    }

}

function logout() {
    firebase.auth().signOut().then(function () {
        console.log('Signed Out');
    }, function (error) {
        console.error('Sign Out Error', error);
    });
}

/* Open the sidenav */
function openNav() {
    if (toolBtnPressed == true){
        closeToolnav();
    }
    document.getElementById("mySidenav").style.width = "200px";
    document.getElementById("main").style.marginLeft = "200px";
    clicked = true;
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    clicked = false;
}