let toolBtnPressed = false;
const toolBtn = document.getElementById("robotSelect");

toolBtn.addEventListener('click', function () {
    if (toolBtnPressed != true) {
        openToolnav();
    } else {
        closeToolnav();
        
    }
});



function openToolnav() {
    if (clicked == true){
        closeNav();
    }
    document.getElementById("myToolnav").style.width = "200px";
    document.getElementById("main").style.marginLeft = "200px";
    toolBtnPressed = true;
    toolBtn.innerHTML = "> " + currentLanguage.data().toolBtn + " >";
}


function closeToolnav() {
    document.getElementById("myToolnav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    toolBtnPressed = false;
    toolBtn.innerHTML = "< " + currentLanguage.data().toolBtn + " <";
}