//Check if user is logged in.
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        showPage("map-page")
        map.invalidateSize();
    } else {
        showPage("login-page");
    }
});

function showPage(pageId) {

    var pages = document.getElementById("main-content").children;

    if (pageId != "login-page") {
        document.getElementById("navbar-content").style.display = "block";
    } else {
        document.getElementById("navbar-content").style.display = "none";
        document.getElementById("main-content").style.display = "none";
        document.getElementById("main").style.display = "none";

    }

    for (var i = 0; i < pages.length; i++) {
        if (pages[i].id != pageId) {
            hidePage(pages[i].id);
            console.log("Page: " + pages[i].id + " hidden")
        }
    }

    document.getElementById(pageId).style.display = "block";
    localStorage.setItem(pageId, true);
    console.log("Showing " + pageId);

}

function hidePage(pageId) {
    document.getElementById(pageId).style.display = "none";
    localStorage.setItem(pageId, false);

}