// HTML Elements
const robotList = document.getElementById("robot-list");
const selectBox = document.getElementById("robot-select");
const unselectButton = document.getElementById("unselectRobot");
const waypointCount = document.getElementById("waypoints");
const robotName = document.getElementById("robot-name");
const robotInfo1 = document.getElementById("robot-raw1");
const robotInfo2 = document.getElementById("robot-raw2");
const pocCheck = document.getElementById("pocrunnerCheck");
const allRobotLoc = document.getElementById("allRobotLoc");
const robotControl = document.getElementById("robotControl");
const robotHeader = document.getElementById("robotHeader");
const emergency = document.getElementById("emergencyButton");
const nameSwitch = document.getElementById("robotNames");
const robotOnOff = document.getElementById("robotIgnite");
const pausePlay = document.getElementById("pausePlay");
const lockView = document.getElementById("lockView");
const mapStyleOption = document.getElementById("basemaps");

///// Map objects
// The map
const map = L.map('mapid', {
    worldCopyJump: true
}).setView([30, 0], 2);

let layer = L.esri.basemapLayer('Topographic').addTo(map);
let layerLabels;



// Custom location icon
const locIcon = L.icon({
    iconUrl: 'img/robot.png',
    shadowUrl: 'img/robotSh.png',
    iconSize: [80, 80], // size of the icon
    shadowSize: [80, 80], // size of the shadow
    iconAnchor: [40, 40], // point of the icon which will correspond to marker's location
    shadowAnchor: [40, 40],  // the same for the shadow
    popupAnchor: [0, -40] // point from which the popup should open relative to the iconAnchor
});
const selectedIcon = L.icon({
    iconUrl: 'img/robotSel.png',
    shadowUrl: 'img/robotSh.png',
    iconSize: [80, 80], // size of the icon
    shadowSize: [80, 80], // size of the shadow
    iconAnchor: [40, 40], // point of the icon which will correspond to marker's location
    shadowAnchor: [40, 40],  // the same for the shadow
    popupAnchor: [0, -40] // point from which the popup should open relative to the iconAnchor
});
// Waypoint location icon
const wpIcon = L.icon({
    iconUrl: 'img/waypoint.png',
    shadowUrl: 'img/wpShadow.png',
    iconSize: [26, 36], // size of the icon
    shadowSize: [26, 36], // size of the shadow
    iconAnchor: [13, 36], // point of the icon which will correspond to marker's location
    shadowAnchor: [13, 36],  // the same for the shadow
    popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor
});
// Ueno's image tracker location icon
const imgLocIcon = L.icon({
    iconUrl: 'img/imgLoc.png',
    shadowUrl: 'img/imgLocShadow.png',
    iconSize: [20, 20], // size of the icon
    shadowSize: [20, 20], // size of the shadow
    iconAnchor: [10, 10], // point of the icon which will correspond to marker's location
    shadowAnchor: [10, 10],  // the same for the shadow
    popupAnchor: [0, 0] // point from which the popup should open relative to the iconAnchor
});
// Map layer groups
const waypointGroup = L.layerGroup().addTo(map);
const ghostGroup = L.layerGroup().addTo(map);
const robotLocation = L.layerGroup().addTo(map);
const popupLayer = L.layerGroup().addTo(map);
const imgLocLayer = L.layerGroup().addTo(map);

/////////// Variables
let wpList = [];
let robotsOnMap = [];
let robots = [];
let wpCounter = 0;
let selectedRobot = null;
let latlngs = Array();
let robotSelected = false;
let robotRenderList = [];
let robotStatusList = [];
let nameShow = true;
let lockedView = false;
let robotStatus;
let statusString = ["Off", "On", "Running", "Pause", "Error"];

//Mapbox
mapboxgl.accessToken = 'pk.eyJ1IjoiYnlsaSIsImEiOiJjam9qazJzbW4wNjF6M3Bxb2lsaWNnY2tjIn0.LiYKCrnaMTdNrp5mQyCIAQ';

// Load map-tiles
// L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
//     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
//     maxZoom: 28,
//     id: 'mapbox.streets-satellite',
//     accessToken: 'pk.eyJ1IjoiYnlsaSIsImEiOiJjam9qazJzbW4wNjF6M3Bxb2lsaWNnY2tjIn0.LiYKCrnaMTdNrp5mQyCIAQ'
// }).addTo(mymap);




// Map-styles (Normal/Satellite)
// document.getElementById("normal-map").addEventListener('click', function () {
//     changeMapStyle("streets");
// });
// document.getElementById("satellite-map").addEventListener('click', function () {
//     changeMapStyle("streets-satellite");
// });

init();



// Listeners
// Listen Firestore for changes in Robot documents. When change occurs, update robot array and render robots on map. 
db.collection("robots").onSnapshot(function (doc) {
    var changes = doc.docChanges();
    changes.forEach(function (change) {
        if (change.type === "added") {

            robotRenderList.push(change.doc);
        }
        if (change.type === "modified") {
            var foundIndex = robotRenderList.findIndex(x => x.id === change.doc.id);
            robotRenderList[foundIndex] = change.doc;
        }
    });
    if (selectedRobot != null) {
        var index = robotRenderList.findIndex(x => x.id === selectedRobot.id);
        showRobotInfo(robotRenderList[index]);
    }

    renderAllRobots(robotRenderList);
    if (selectedRobot != null) {
        toggleStatusChangers(selectedRobot);

        if (lockedView == true) {
            zoomToRobot(selectedRobot, null);
        }
    }
});

db.collection("robotStatus").onSnapshot(function (doc) {
    var changes = doc.docChanges();
    changes.forEach(function (change) {
        if (change.type === "added") {
            db.collection("robots").doc(change.doc.id).update(change.doc.data());
        }
        if (change.type === "modified") {
            db.collection("robots").doc(change.doc.id).update(change.doc.data());
        }
    });

});

// db.collection("robotStatus").onSnapshot(function (doc) {
//     var changes = doc.docChanges();
//     changes.forEach(function (change) {
//         if (change.type === "added") {
//             console.log("New : ", change.doc.data());
//             robotStatusList.push(change.doc);
//         }
//         if (change.type === "modified") {
//             var foundIndex = robotStatusList.findIndex(x => x.id === change.doc.id);
//             robotStatusList[foundIndex] = change.doc;
//         }
//         if (change.type === "removed") {
//             console.log("Removed : ", change.doc.data());
//         }
//     });
//     if (selectedRobot != null) {
//         var index = robotRenderList.findIndex(x => x.id === selectedRobot.id);
//         showRobotInfo(robotRenderList[index]);
//     }

//     renderAllRobots(robotRenderList);
//     if (selectedRobot != null) {
//         toggleStatusChangers(selectedRobot);

//         if (lockedView == true) {
//             zoomToRobot(selectedRobot);
//         }
//     }
// });



// Eventlistener for robot selection element
selectBox.addEventListener("change", function () {
    clearAll();
    if (this.value != "") {
        selectedRobot = db.collection("robots").doc(this.value);


        if (selectedRobot != null) {

            showRobotView(selectedRobot);


        }
        map.on('click', setWaypoints);
    } else {
        defaultMapPosition();
        hideRobotInfo();
        clearAll();
    }
    renderAllRobots(robotRenderList);
});

// mapStyleOption.addEventListener("change", function () {

// });


// Eventlistener for unselect button
unselectButton.addEventListener('click', function () {
    selectBox.value = "";
    defaultMapPosition();
    hideRobotInfo();
    clearAll();
    renderAllRobots(robotRenderList);

});

// Event listener for Ueno's Image Tracker
pocCheck.addEventListener("change", function () {
    if (this.checked) {
        showImageLocIcon();
    } else {
        imgLocLayer.clearLayers();
    }
})

nameSwitch.addEventListener("change", function () {
    if (this.checked) {
        nameShow = true;
    } else {
        nameShow = false;
    }
    renderAllRobots(robotRenderList);
})

robotOnOff.addEventListener("change", function () {
    if (this.checked) {
        console.log("Robot ON")
        updateStatus(selectedRobot.id, 1);
        emergency.innerHTML = "! STOP " + selectedRobot.id + " !";
        emergency.style.display = "block";
    } else {
        console.log("Robot OFF")
        updateStatus(selectedRobot.id, 0);
        emergency.style.display = "none";
    }
})

emergency.addEventListener('click', function () {
    updateStatus(selectedRobot.id, 0);
    alert("EMERGENCY STOP: " + selectedRobot.id);
    zoomToRobot(selectedRobot, null);
    robotOnOff.checked = false;
    emergency.style.display = "none";
});

pausePlay.addEventListener('click', function () {
    if (robotStatus == 2) {
        updateStatus(selectedRobot.id, 3);
    }

    else if (robotStatus == 3) {
        updateStatus(selectedRobot.id, 2);
    }

});

lockView.addEventListener("change", function () {
    if (this.checked) {
        lockedView = true;
    } else {
        lockedView = false;
    }
})

function setBasemap(basemap) {
    if (layer) {
        map.removeLayer(layer);
    }

    layer = L.esri.basemapLayer(basemap);

    map.addLayer(layer);

    if (layerLabels) {
        map.removeLayer(layerLabels);
    }

    if (basemap === 'ShadedRelief'
        || basemap === 'Oceans'
        || basemap === 'Gray'
        || basemap === 'DarkGray'
        || basemap === 'Terrain'
    ) {
        layerLabels = L.esri.basemapLayer(basemap + 'Labels');
        map.addLayer(layerLabels);
    } else if (basemap.includes('Imagery')) {
        layerLabels = L.esri.basemapLayer('ImageryLabels');
        map.addLayer(layerLabels);
    } 
}

function changeBasemap(basemaps) {
    var basemap = basemaps.value;
    setBasemap(basemap);
}

function renderGhostWaypoints(robotId) {
    ghostGroup.clearLayers();
    console.log(robotId)
    db.collection("waypoints").where("name", "==", robotId).orderBy("timestamp", "desc").limit(1).get()
        .then(function (querySnapshot) {
            querySnapshot.forEach(function (doc) {
                console.log(doc.id, " => ", doc.data().waypoint.length);

                L.polyline(doc.data().waypoint, { color: "gray" }).addTo(ghostGroup);
                for (var i = 0; i < doc.data().waypoint.length; i++) {

                    console.log(doc.data().waypoint[i].lat + " " + doc.data().waypoint[i].lng);

                    L.circle([doc.data().waypoint[i].lat, doc.data().waypoint[i].lng], {
                        color: "pink",
                        fillColor: 'pink',
                        fillOpacity: 0.8,
                        radius: 2
                    }).addTo(ghostGroup);
                }
            });
        })
        .catch(function (error) {
            console.log("Error getting documents: ", error);
        });
}

function updateStatus(robotId, value) {
    db.collection("robotStatus").doc(robotId).set({
        stat: value
    })
}

function showRobotView(selectedRobot) {
    var index = robotRenderList.findIndex(x => x.id === selectedRobot.id);
    showRobotInfo(robotRenderList[index]);
    toggleStatusChangers(selectedRobot);
    zoomToRobot(selectedRobot, null)
    renderGhostWaypoints(selectedRobot.id);
}


function toggleStatusChangers(selectedRobot) {
    var status;
    selectedRobot.get().then(function (doc) {
        if (doc.exists) {
            pausePlay.style.backgroundColor = "lightgray";
            robotStatus = doc.data().stat;

            if (robotStatus > 0) {
                robotOnOff.checked = true;
                emergency.innerHTML = "! STOP " + selectedRobot.id + " !";
                emergency.style.display = "block";
            } else {
                robotOnOff.checked = false;
                emergency.style.display = "none";
            }

            if (robotStatus == 2) {
                pausePlay.disabled = false;
                pausePlay.innerHTML = "❚❚";
                pausePlay.style.backgroundColor = "lightyellow";
            } else if (robotStatus == 3) {
                pausePlay.disabled = false;
                pausePlay.innerHTML = "►";
                pausePlay.style.backgroundColor = "lightgreen";
            } else {
                pausePlay.innerHTML = "►/❚❚";
                pausePlay.disabled = true;

            }

            robotHeader.innerHTML = selectedRobot.id;

            robotControl.style.display = "block";


        } else {
            console.log("No such document!");
        }
    }).catch(function (error) {
        console.log("Error getting document:", error);
    });


}

// Zoom to robot, when robot is selected from the list.
function zoomToRobot(selectedRobot, zoomLevel) {
    selectedRobot.get().then(function (doc) {
        if (doc.exists) {
            if (zoomLevel != null) {
                map.setView([doc.data().lat, doc.data().lng], zoomLevel);
            } else {
                map.setView([doc.data().lat, doc.data().lng]);
            }

        } else {
            console.log("No such document!");
        }
    }).catch(function (error) {
        console.log("Error getting document:", error);
    });

}

function init() {
    map.invalidateSize();
    emergency.style.display = "none";
    robotName.style.display = "none";
    robotControl.style.display = "none";
    showRobotList();

}

// Set map to default zoom
function defaultMapPosition() {
    map.setView([30, 0], 2);
}

// Listen Firestore for selected robot's location. 
function listenRobotLocation(robotId) {
    db.collection("robots").doc(robotId)
        .onSnapshot(function (doc) {
            console.log("Current data: ", doc.data());
            console.log(robotId + "Toka")
            renderRobot(doc);
            showRobotInfo(doc);
        });

}

// Get Ueno's Image Tracker image location information from Firestore. Render locations on map. 
function showImageLocIcon() {
    db.collection('images').get().then((snapshot) => {
        snapshot.docs.forEach(doc => {
            const data = doc.data();
            for (var i = 0; i < Object.keys(data).length; i++) {
                L.circle([data[i].latitude, data[i].longitude], {
                    color: "#000000",
                    fillColor: ' #66ff66',
                    fillOpacity: 0.7,
                    radius: 2.5
                }).addTo(imgLocLayer);
            }

        })
    });
}


// Hide robot information from gui
function hideRobotInfo() {
    selectedRobot = null;
    robotHeader.innerHTML = "";
    emergency.style.display = "none";
    robotControl.style.display = "none";
    robotName.style.display = "none";
    robotName.innerHTML = "";
    robotInfo1.innerHTML = "";
    robotInfo2.innerHTML = "";
}

// Get robots from firestore, render robotlist. 
function showRobotList() {
    db.collection('robots').get().then((snapshot) => {
        snapshot.docs.forEach(doc => {
            robots.push(doc.id);
            renderRobotList(doc);
        })
    });
}

function changeRobot(robotId) {
    clearAll();
    selectedRobot = db.collection("robots").doc(robotId);
    selectBox.value = selectedRobot.id;
    showRobotView(selectedRobot);
    map.on('click', setWaypoints);
    renderAllRobots(robotRenderList);
}

// Render single robot on map. 
function renderRobot(doc) {
    let docName = doc.id;
    let marker;


    if (doc.data().lat != null && doc.data().lng != null) {


        if (selectedRobot != null) {
            if (selectedRobot.id == docName) {
                marker = L.marker([doc.data().lat, doc.data().lng], { icon: selectedIcon }).addTo(robotLocation);
            }
        }

        if (marker == null) {
            marker = L.marker([doc.data().lat, doc.data().lng], { icon: locIcon }).addTo(robotLocation);
        }

        marker.addEventListener("click", function () {
            changeRobot(docName);
        });



        marker.bindTooltip(docName, {
            direction: top,
            permanent: nameShow
        });

    }
}




// Render all robots on map.
function renderAllRobots(robots) {
    robotLocation.clearLayers();
    for (var i = 0; i < robots.length; i++) {
        renderRobot(robots[i]);

    }

}

// Render selected robot's information. 
function showRobotInfo(doc) {

    let status = statusString[doc.data().stat];
    robotName.style.display = "block";
    robotName.innerHTML = "✛ " + doc.id;
    robotInfo1.innerHTML = "Status: " + status + " - Speed: " + doc.data().spd.toFixed(1)
    robotInfo2.innerHTML = "Lat: " + doc.data().lat + " Lng: " + doc.data().lng + " Compass: " + doc.data().cmps
}

// Reset
function clearAll() {
    wpList = [];
    waypointGroup.clearLayers();
    wpCounter = 0;
    map.closePopup();
    latlngs = Array();
}

// Function for clicking the map. Used for waypoints
function setWaypoints(e) {
    if (selectedRobot != null) {

        selectedRobot.get().then(function (doc) {
            if (doc.exists) {
                if (doc.data().stat > 0) {
                    const marker = new L.marker(e.latlng, { icon: wpIcon }).addTo(waypointGroup);
                    const lat = e.latlng.lat;
                    const lng = e.latlng.lng;

                    wpCounter = wpCounter + 1;
                    renderWaypoints(e.latlng);

                    latlngs.push(e.latlng);

                    const polyline = L.polyline(latlngs, { color: "magenta" }).addTo(waypointGroup);

                    wpList.push({ "lat": lat, "lng": lng });

                } else {
                    alert(selectedRobot.id + " is OFF! Turn ON to set waypoints.")
                }

            } else {
                console.log("No such document!");
            }
        }).catch(function (error) {
            console.log("Error getting document:", error);
        });


    }
}

// Render robotlist. 
function renderRobotList(doc) {

    const z = document.createElement("option");
    const textNode = document.createTextNode(doc.id);
    z.setAttribute("value", doc.id);
    z.appendChild(textNode);
    selectBox.appendChild(z);
}

// Change mapstyle. 
function changeMapStyle(style) {
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 28,
        id: 'mapbox.' + style,
        accessToken: 'pk.eyJ1IjoiYnlsaSIsImEiOiJjam9qazJzbW4wNjF6M3Bxb2lsaWNnY2tjIn0.LiYKCrnaMTdNrp5mQyCIAQ'
    }).addTo(map);
}

// Push set waypoints to Firestore
function pushWaypoints(waypoints) {

    db.collection('waypoints').add({
        timestamp: Date.now(),
        name: selectedRobot.id,
        waypoint: waypoints
    });
    clearAll();
    renderGhostWaypoints(selectedRobot.id);
}

function patrolList(waypointList, multiplier) {

    let patrolRoute = waypointList;
    for (var i = 1; i < multiplier; i++) {
        patrolRoute = patrolRoute.concat(waypointList);
    }

    console.log(patrolRoute);
    return patrolRoute;
}


// Render set waypoints on map
function renderWaypoints(latlng) {
    let popup = new L.popup();
    popup.setLatLng(latlng).setContent(
        "<button id='wpPush'>" + pushText + " [" + wpCounter + "]" + "</button>" +
        "<br>" + "<div id='patrolSliderContainer'>" + "<input type='number' min='1' max='100' step='1' placeholder='1-100' class='patrolSlider' id='patrolRange'>" + "<button id='patrolBtn'>Patrol</button>" + "</div>" +
        "<br>" + "<button id='wpClear' onclick='clearAll()'>" + clearText + "</button>" +
        "<br>" + latlng
    ).openOn(map);

    document.getElementById("wpPush").addEventListener('click', function () {

        if (confirm("Robot: " + selectedRobot.id + "\nWaypoints: " + wpList.length)) {
            pushWaypoints(wpList);
        }


    });


    document.getElementById("patrolBtn").addEventListener('click', function () {
        let multiplierValue = Math.round(document.getElementById("patrolRange").value);
        console.log(multiplierValue);

        if (wpList.length > 1) {

            if (multiplierValue == "" || multiplierValue < 1) {
                multiplierValue = 1;
            }
            if (multiplierValue > 100) {
                multiplierValue = 100;
            }

            if (confirm("Robot: " + selectedRobot.id + "\nWaypoints: " + wpList.length + "\nIterations: " + multiplierValue)) {
                let patrol = patrolList(wpList, multiplierValue)
                pushWaypoints(patrol);
                console.log("Waypoints pushed to database!");

            }

        } else {
            alert("Patrol-feature needs more than " + wpList.length + " waypoints.");
        }

    });

}

